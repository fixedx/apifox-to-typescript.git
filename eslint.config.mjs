import js from '@eslint/js';
import globals from 'globals';

export default [
    js.configs.recommended,
    {
        languageOptions: {
            ecmaVersion: 2022,
            sourceType: "module",
            globals: {
                ...globals.node,
                myCustomGlobal: "readonly"
            }
        },
        rules: {
            "linebreak-style": ["error", "unix"]
        }
        // ...other config
    },
    {
        ignores: ["./lib/*"]
    }
]