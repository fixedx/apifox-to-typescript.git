/* eslint-disable */
// @ts-nocheck
import req from '{{ requestMethodPath }}'
import { AxiosRequestConfig } from 'axios';

export const apiId = '{{ apiId }}';

/**
* {{ name }}
*/

{{#each interfaces}}
export interface {{name}} {
    {{#each fields}}
    /**
    *  {{ description }}
    */
    "{{name}}"{{#unless required}}?{{/unless}}: {{type}};
    
    {{/each}}
}
{{/each}}

export function request(config?: Omit<AxiosRequestConfig<RequestBody>, "params"> & {params?: RequestQuery}) {
    return req<RequestBody, RequestQuery, ResponseBody, ResponseBody["data"]>(`{{path}}`, {
        method: '{{ method }}',
        mockUrl: '{{ mockUrl }}',
        ...config
    });
}

