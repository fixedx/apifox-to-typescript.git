const path = require("path");
const { setToken, request } = require("../utils/request");

const { apifox = {} } = require(path.join(process.cwd(), 'apifox.config.js'));

module.exports = async function login() {
    try {
        if (apifox.auth.token) {
            setToken(apifox.auth.token);
            return;
        }
        const res = await request({
            url: '/login',
            method: 'post',
            data: {
                ...apifox.auth,
            }
        });
        setToken(res.accessToken);
    } catch (error) {
        console.error('login error: ', error);
    }
}